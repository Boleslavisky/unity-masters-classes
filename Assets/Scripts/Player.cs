﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private float speed;
    public GameObject Bala;

    private bool Grapping;
    private bool punching;

    private float punchTime;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {      // transform.Translate(new Vector3(speed, 0, 0));
            //GetComponent<Rigidbody>().AddForce(new Vector3(speed, 0, 0));
            GetComponent<CharacterController>().Move(new Vector3(speed, 0, 0));
        }
        if (Input.GetKey(KeyCode.LeftArrow))

        {
            //transform.Translate(new Vector3(-speed, 0, 0));
            //GetComponent<Rigidbody>().AddForce(new Vector3(-speed, 0, 0));
            GetComponent<CharacterController>().Move(new Vector3(-speed, 0, 0));
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            //transform.Translate(new Vector3(0, 0, speed));
            //GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, speed));
            GetComponent<CharacterController>().Move(new Vector3(0, 0, speed));
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            //transform.Translate(new Vector3(0, 0, -speed));
            //GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, -speed));
            GetComponent<CharacterController>().Move(new Vector3(0, 0, -speed));
        }
        if (Input.GetKey(KeyCode.Space))
        {
            //transform.Translate(new Vector3(0, speed*5, 0));
            //GetComponent<Rigidbody>().AddForce(new Vector3(0, speed, 0));
            GetComponent<CharacterController>().Move(new Vector3(0, 0, 0));

        }
        if (!punching)
        {
            if (Input.GetMouseButton(0) && !Grapping)
            {
                Punch();
            }
            if (Input.GetMouseButtonUp(0))
            {
                if (Grapping)
                {
                    ReleaseHold();
                }
            }
            else if (Input.GetMouseButton(0))
            {
                Grab();
            }            
        }
        if (Time.timeSinceLevelLoad > (punchTime + 1))
        {
            if (punching)
            {
                print("Stopped punching");
                punching = false;
            }
        }
    }

    private void ReleaseHold()
    {
        print("releasing grab");
        Grapping = false;
    }

    private void Grab()
    {
        Grapping = true;
        print("Holding");
    }

    private void Punch()
    {
        punching = true;
        print("Punching");
        punchTime = Time.timeSinceLevelLoad;
    }
}
