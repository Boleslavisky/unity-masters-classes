﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Mut : MonoBehaviour
{
    public UnityEvent Event;
    public KeyCode button;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(button)) 
        {
            Event?.Invoke();
        }

    }
}
