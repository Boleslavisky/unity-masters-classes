﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCube : MonoBehaviour
{
    public Rigidbody EnemyCube;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            Rigidbody enemy = Instantiate(EnemyCube, transform.position, Quaternion.identity);
            enemy.GetComponent<EnemyCube>().player = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }
}
