﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class CylinderEnemy : MonoBehaviour
{
    Vector3 newPosition;
    public GameObject Bala;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame

    void FixedUpdate()
    {
        newPosition = Vector3.MoveTowards(transform.position, player.transform.position, 0.1f);

        transform.position = newPosition;
        if (Input.GetKey(KeyCode.Space))

        {

            Instantiate(Bala, gameObject.transform.position, Quaternion.identity);
            //transform.localScale = new Vector3(Vector3.Distance(transform.position,player.transform.position)*0.1f, 0, 0);
        }
    }
}
