﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletClicker : MonoBehaviour
{
    public float SpawnDistance;
    public GameObject Bullet;
    public float BulletSpeed;
    // Start is called before the first frame update
    void Start()
    {


         
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = SpawnDistance;
            Vector3 objectPos = Camera.main.ScreenToWorldPoint(mousePos);
            GameObject MyBullet = Instantiate(Bullet, objectPos, Quaternion.identity);
            MyBullet.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, BulletSpeed));
            

        }
    }
}
